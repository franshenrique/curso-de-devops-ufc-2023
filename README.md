# Curso de DevOps e infraestrutura como código 2023.2 

## Atividade 1 - IaC e git

O intuito dessa atividade é praticar a utilização do git e introduzir a Infrastructure as Code (IaC)

## Atividade 2 - Docker

Nessa atividade será praticado a utilização de Docker e conceitos como:
* build
* images
* container registry
* volumes
* bind
* compose
* multistage build